# OSM-apresentacoes

Slides e materiais suplementares de apresentações públicas realizadas por membros da Comunidade OSM Brasil.

Videos dos eventos em [youtube.com/channel/UCzYgQtC...](https://www.youtube.com/channel/UCzYgQtCudy1ihZYAlpC3EXQ)
## State of Map LatAm 2016
[Evento ocorrido em novembro de 2016](https://wiki.openstreetmap.org/wiki/State_of_the_Map_Latam_2016). Documentos na pasta [SoM-LamTam2016](SoM-LamTam2016)



1. CRP - Código de Roteamento Postal.  [video](https://www.youtube.com/watch?v=rVAmss9An_Q) ... [slides](SoM-LamTam2016/CRP-CodRoteamentoPostal)...
2. ...

## State of Map LatAm 2018
[Evento ocorrido em setembro de 2018](https://wiki.openstreetmap.org/wiki/State_of_the_Map_Latam_2018).

1. [Video](https://www.youtube.com/watch?v=llOycVeZ1hY), slides ...
2. ...
3. 