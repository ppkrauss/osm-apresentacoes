CRP - Código de Roteamento Postal
transformando o CEP em domínio público
Peter Krauss
https://github.com/OSMBrasil/CRP
(apresentado em novembro de 2016)

No Brasil adotamos como recomendação técnica do governo federal o uso do sistema de endereçamento postal que se baseia no CEP: Código de Endereçamento Postal.
Cartas, placas de rua, etc. adotam o CEP como “identificador da localidade”.
Como a entrega da carta não é garantida sem CEP, é de fato uma obrigação.
Sim, ele mesmo, o CEP dos correios!


“Norma voluntária”
Normas técnicas ABNT, recomendações W3C, RFC
ABNT, W3C, RFC
Pode ter ©... certo ser livre.
O CEP dos correios brasileiros
“Norma de obrigação”
Leis, Decretos, Portarias... ou normas técnicas citadas na regra da obrigação.
Federal, Estadual, MunicipalDomínio Público (a Constituição exige)
 
Exemplo:
Autoridade:
O CEP é citado direta e indiretamente em dezenas de Leis, Decretos e portarias: caracteriza-se como obrigação.
Licença:

O CEP dos correios brasileiros
Nas placas.
Não é exclusividade de São Paulo, como mostra esta placa de cidade do interior do estado de SC (CEP 63010) ou de Fortaleza                       (CEP 60030).

Quando não sabemos o nome da coisa, apontamos com o dedo!
Os identificadores se prestam tanto 1) a batizar o que ainda não tem nome, como a  2) permitir que os sistemas digitais “apontem” para qualquer objeto digitalmente.

Nomes e identificadores

Quem é o “dono” dos nomes e identificadores geo?
Ruas e demais logradouros, no Brasil, são “batizados” e seus nomes mantidos pelas Câmaras Municipais: o “dono do nome da rua” é o município, e a autoridade (para mudar ou dizer como se escreve) é a Câmara. As Leis Municipais estabelecem a grafia correta, e isso fica gravado como dado de domínio público no Diário Oficial e bancos de dados do governo.
Além de nomes de rua, existe a responsabilidade pelos nomes de bairro e, numa escala intermediária, o “batismo de CEP” nos novos loteamentos.          Assim o CEP é tanto um patrimônio de utilidade pública como,por indução, um identificador cujo “dono” é o município.

O CEP dos correios brasileiros

No Brasil, até 1992 os códigos de CEP eram compostos de 5 dígitos, correspondendo a uma hierarquia de região/sub-região/setor/etc. e a um “mapa do CEP”. 
Estes mapas são ainda hoje de domínio público, poque não estão no OSM?? 
Os “identificadores de distribuição” (sufixo de 3 dígitos) vieram depois, já numa época em que a agência ETC se preparava para “privatizar” o sistema.

Apenas o acesso individual ao CEP é público!
Por haver um serviço razoável de “resolução de CEP”, a percepção geral do cidadão é de que seria um dado público.

Mas não é, e quem de fato toma susto são os programadores e analistas de sistamas que precisam implantar serviços sérios e competentes de geocodificação e validação de endereços... Importante discussão entre esses profissionais ressurgiu com mais força em 
 http://pt.stackoverflow.com/a/56664

lá foram consolidados os principais argumentos que justificam a colocação imediata do CEP em domínio público.

Apenas o acesso individual ao CEP é público!
Problemas criados por essa restrição:
Não funciona fora da Internet;
Fica muito lento em horários de pico.
Não permite competição de mercado;
Não podemos fazer validação em lote de endereços;
Não podemos fazer mapas derivados, estatísticas, otimizações, etc. etc. etc.


... E o problema não é só do Brasil!
Código Postal Aberto em:

Ruanda, Colômbia, Holanda, Uruguai, Finlândia, Islandia, Marrocos, Japão, ...

... +- no México, ...


Código Postal Fechado em:

Brasil, Paraguais, Panama, Siria, Canadá, Sudão, ... 

... e dezenas de ditaturas e países do terceiro mundo.



... E o problema não é só do Brasil!
Código Postal Aberto, ver http://index.okfn.org




... E o problema não é só do Brasil!
Código Postal Fechado, ver http://index.okfn.org




Este link por exemplo, http://www.fileshare.ro/e31477206 foi atualizado em 2015, e desde então se mantêm online sem qualquer reclamação por parte do governo ou da ECT. Dois motivos para não reclamarem: 1) como se vê abaixo, não é o formato da base oficial, logo não é cópia; 2) se levar na justiça há um risco de cair nas mãos de um juiz mais consciente dos fatos e inverter o jogo. 
O que existe na Web é pirataria não-punida
CEP;Tipo_Logradouro;Logradouro;Complemento;Local;Bairro;Cidade;UF;Estado
01001000;Praça;da Sé;- lado ímpar;;Sé;São Paulo;SP;São Paulo
01001001;Praça;da Sé;- lado par;;Sé;São Paulo;SP;São Paulo
01001010;Rua;Filipe de Oliveira;;;Sé;São Paulo;SP;São Paulo
01001900;;Praça da Sé 108;;UNESP - Universidade Estadual Júlio de Mesquita Filho;Sé;São Paulo;SP;São Paulo
01001901;;Praça da Sé 371;;Edifício Santa Lídia;Sé;São Paulo;SP;São Paulo
01001902;;Praça da Sé 385;;OAB - Ordem dos Advogados do Brasil.;Sé;São Paulo;SP;São Paulo
01002000;Rua;Direita;- lado par;;Sé;São Paulo;SP;São Paulo
01002001;Rua;Direita;- lado ímpar;;Sé;São Paulo;SP;São Paulo


Inconstitucionalidade da proteção da lista de CEPs
No Brasil, pela Constituição, e pelo Tratado de Berna, são são de domínio público:
os documentos contendo legislação, tais como a própria Constituição da República, todas as leis, os decretos, portarias, etc.
Os apêndices, listagens, anexos, tabelas, mapas, etc. destes documentos.
Mesmo não-publicados ou não-atualizados, porém citados como parte das obrigações do cidadão.

Uma coisa de cada vez... Os juízes raramente aceitam tudo de uma das partes neste tipo de disputa.  O mais provável é conseguir o menos defensável de “autoria”: a lista dos códigos dos CEPs de cada município num arquivo TXT.
Empresas e instituições fazem uso de bases de endereços, não apenas de CEPs, como ilustra o diagrama UML.
O que é a base, e o que precisamos ter publicamente

1 a 3 dígitos definem unidades da federação
CEPs do Maranhão (MA) prefixo 65, do Mato Grosso do Sul (MS) 79...
Nas unidades mais populosas, um dígito: do Rio Grande do Sul (RS) 9, Minas Gerais (MG) 3...
Nas menos populosas, três: Acre (AC) 699, Roraima (RR) 693,  Amapá (AP) 689...
E algumas variações para otimizar o uso dos dígitos com relação à pop. estimada.




PROJETO CRP, SOLUÇÃO PARA A OSM LIBERTAR O CEP!

 códigos-fonte  https://github.com/OSMBrasil/CRP
 demonstração https://OSMBrasil.github.io/CRP

FINALIDADE: estabelecer as convenções para se transcrever a string de CEP no formato CRP, e demonstrar que a convenção é consistente, simples e reversível... E usar isso no OpenStreetMap-BR!





